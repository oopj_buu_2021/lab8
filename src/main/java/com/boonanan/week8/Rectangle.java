package com.boonanan.week8;

public class Rectangle {
    private String name;
    private int width;
    private int height;

    public Rectangle(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;

    }
    public String getNameRec() {
        return name;
    }
    public int getWidthRec() {
        return width;
    }
    public int getHeightRec() {
        return height;
    }

    void recArea() {
        System.out.println((name) + (" Area = ") + (width * height));
    }
    void recPerimeter() {
        System.out.println((name) + (" Perimeter = ") + (2*(width + height)));
    }
    void recInfo() {
        System.out.println("Name:" + name + " Width:" + width + " Height:" + height);
    }
}
