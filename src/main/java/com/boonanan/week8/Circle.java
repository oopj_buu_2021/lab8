package com.boonanan.week8;

public class Circle {
    private String name;
    private int radius;

    public Circle(String name, int radius) {
        this.name = name;
        this.radius = radius;

    }
    public String getNameCir() {
        return name;
    }
    public int getRadiusCir() {
        return radius;
    }

    void cirArea() {
        System.out.println((name) + (" Area = ") + (Math.PI * (radius*radius)));
    }
    void cirPerimeter() {
        System.out.println((name) + (" Perimeter = ") + (2*Math.PI*radius));
    }
    void cirInfo() {
        System.out.println("Name:" + name + " Radius:" + radius);
    }

}
