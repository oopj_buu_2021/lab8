package com.boonanan.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CirTest {
    @Test
    public void correctName() {
        Circle circle1 = new Circle("circle1", 10);
        assertEquals("circle1", circle1.getNameCir());
    }
    @Test
    public void correctRadius() {
        Circle circle1 = new Circle("circle1", 10);
        assertEquals(10, circle1.getRadiusCir());
    }

    @Test
    public void correctArea() {
        Circle circle1 = new Circle("circle1", 10);
        int area = (int) (Math.PI*(circle1.getRadiusCir()*circle1.getRadiusCir()));
        assertEquals(314, area);
    }

    @Test
    public void correctPerimeter() {
        Circle circle1 = new Circle("circle1", 10);
        int perimeter = (int) (2 * Math.PI*circle1.getRadiusCir());
        assertEquals(62, perimeter);
    }
}
