package com.boonanan.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TriTest {
    @Test
    public void correctName() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        assertEquals("triangle", triangle.getNameTri());
    }
    @Test
    public void correctA() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        assertEquals(5, triangle.getATri());
    }
    @Test
    public void correctB() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        assertEquals(5, triangle.getBTri());
    }
    @Test
    public void correctC() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        assertEquals(6, triangle.getCTri());
    }
    @Test
    public void correctArea() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        int s = (triangle.getATri() + triangle.getBTri() + triangle.getCTri()) / 2;
        int area = (int) Math.sqrt(s * (s - triangle.getATri()) * (s - triangle.getBTri()) * (s - triangle.getCTri()));
        assertEquals(12, area);
    }
    @Test
    public void correctPerimeter() {
        Triangle triangle = new Triangle("triangle",5,5,6);
        int Perimeter = triangle.getATri() + triangle.getBTri() + triangle.getCTri();
        assertEquals(16, Perimeter);
    }
}
