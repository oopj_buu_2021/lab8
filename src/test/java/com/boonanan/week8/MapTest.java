package com.boonanan.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MapTest {
    @Test
    public void correctHeight1() {
        Map map = new Map();
        assertEquals(5, map.getHeight());
    }
    @Test
    public void correctWidth1() {
        Map map = new Map();
        assertEquals(5, map.getWidth());
    }
    @Test
    public void correctHeight2() {
        Map map = new Map(40,20);
        assertEquals(20, map.getHeight());
    }
    @Test
    public void correctWidth2() {
        Map map = new Map(40,20);
        assertEquals(40, map.getWidth());
    }
}
