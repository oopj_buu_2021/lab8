package com.boonanan.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RecTest {
    @Test
    public void correctName() {
        Rectangle rec1 = new Rectangle("rec1", 10, 5);
        assertEquals("rec1", rec1.getNameRec());
    }
    @Test
    public void correctWidth() {
        Rectangle rec1 = new Rectangle("rec1", 10, 5);
        assertEquals(10, rec1.getWidthRec());
    }
    @Test
    public void correctHeight() {
        Rectangle rec1 = new Rectangle("rec1", 10, 5);
        assertEquals(5, rec1.getHeightRec());
    }

    @Test
    public void correctArea() {
        Rectangle rec1 = new Rectangle("rec1", 10, 5);
        assertEquals(50, rec1.getHeightRec()*rec1.getWidthRec());
    }

    @Test
    public void correctPerimeter() {
        Rectangle rec1 = new Rectangle("rec1", 10, 5);
        assertEquals(30, 2*(rec1.getHeightRec()+rec1.getWidthRec()));
    }
}
