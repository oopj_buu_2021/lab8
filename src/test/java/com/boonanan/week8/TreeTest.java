package com.boonanan.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TreeTest {
    @Test
    public void correctName() {
        Tree tree = new Tree("tree",9,7);
        assertEquals("tree", tree.getName());
    }

    @Test
    public void correctX() {
        Tree tree = new Tree("tree",9,7);
        assertEquals(9, tree.getX());
    }

    @Test
    public void correctY() {
        Tree tree = new Tree("tree",9,7);
        assertEquals(7, tree.getY());
    }
}
